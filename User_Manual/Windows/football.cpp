/*
 *  This is the MVP program for group 13. It is a football tournament that allows ussers.
 *  The program is not finished and is stil missing the features:
 *  - Able to log in to different users (either normal user or as admin)
 *  - Save a unfinished tournament, just finished touraments with a winner will be saved
 *  - Able to decide how many goals each team scord in the matches (Will be implemented in the next version)
 *
 *  What the program does so far:
 *  - Creating a new tourament, and automaticly asssigns a teams from the global vector
 *  - Run a tournament that has been created. The program will automaticly know if there are 4, 2 or 1 match
 *  that will be played
 *  - Able to see the created tournaments and what stage of the tourment they are in (4, 2 or 1 match)
 *  - See the results of all the teams that have won. Will be stored in file
 *
 *  Features to come:
 *  - Save a unfinished tourament
 *  - Login to a normal user or a Admin user
 *  - Decide how many goals each team scored in the matches
 *  - Add new teams that can join the tourament (might add so u can choose who is going to play (with enum?))
 *
 * //NOTE - having a lot of problems with pure functions, probably doing something wrong.
 *
 *  PROG1004_Team 13
 */

#include <iostream>
#include <string>
#include <iomanip>
#include "LesData2.h"
#include <vector>
#include <random>
#include <algorithm>
#include <fstream>
#include <list>

using namespace std;

    //All the teams that can play
    vector <string> gFotballLag = {"Manchester United", "Manchester City",
    "Aresenal", "Chelsea", "Leeds", "Gjovik FK", "Sarpsborg 08",
    "Westham", "Tottenham", "Fredrikstad", "Moss", "Harstad", "G2"};

class Lag{
    private:
        string  navn,
                mvp;
         int    goals;

    public:
        Lag(const int nr){
            navn = gFotballLag[nr];
            mvp = "";
            goals = 0;
        }
        void skrivData(){
            cout << left << endl;
            cout << setw(25) << "Team name:       " << navn << endl;
            cout << setw(25) << "Goals scored last match:    " << goals << endl;
            cout << endl;
        }
        void lesData(){     //FEATURE, to manually read in name, goals, will be implemented soon

        }

        int lesGoals(){           //returnerer innskrevet verdi for å sjekke om de er like
            int goals;
            cout << "\nHow many goals did " << navn << " score";
            goals = lesInt(":", 0, 8);
            this->goals = goals;
            return this->goals;     // :) can u even code?
        }

        //int goals() const { return(goals); }      //Pure function //PURE, BEAST MODE, fungerer ikke, IDK

        int returnGoals() const { return goals; }
        string returnName() const { return navn; }
        //NOTE får ikke PURE functions til å fungere
        //Pure funksjon for å returnere goals slik at man kan unngå at kampene blir uavgjort

        void lesFraFil(ifstream & in);      //Not in use at the moment
        void skrivTilFil(ofstream & ut);    //Not in use at the moment
};

class Kamp{
    private:
        vector <Lag*> kampLag;

    public:
        Kamp(const int nr){
        int tall = nr * 2;
        int i = 0;
        int max = 0;

            if(nr == 0){}               //This is shit code, dont ask, will change for a better for loop later
            else if(nr == 1){i = 2; }
            else if(nr == 2){i = 4; }
            else{i = 6; }

            max = i + 2;

            for(i; i < max; i++){       // Det å sette ant mål til lag nå er dumt, b
                                        // Bør settes til 0 og når kampene gjøres bør systemet spørre om mål
            cout << "ant: " << i;
            Lag *lagP; lagP = new Lag(i);
            kampLag.push_back(lagP);

            }
        }
        Kamp(Lag* lag1, Lag* lag2);

        virtual void skrivData(){
            kampLag[0]->skrivData();
            cout << "\t\tVS\n";
            kampLag[1]->skrivData();
        }

        Lag* sjekkVinner();         //Returnerer 1 av 2 lag

        virtual void lesData(){};
        void bestemMaal();

};

class Turnering{
    private:
        string host;                //Name of where the tournament is held
        vector <Kamp*> kamper;      //Array of all the matches
        bool slutt;                 //Sjekker om avsluttet
        //int aar;                  //FEATURE, whitch year it was held

    public:

        Turnering();               //Constructor to make new tournament

        int returnKamperSize() const { return kamper.size(); }

        void kjorTurnering();
        void spillKamp(const int ant);  //Parameter sendes med for å sjekke hvor mange kamper som skal spilles
        virtual void skrivData();       //MORE må endret for å gjøres smart
        virtual void lesData(){};
        void skrivNavn();
        int hostNameLength();
        void goalsForKamp(const int nr);
};

class Resultat{                         //To store result of tournamets
    private:
        string  navn;
        int     antSeiere;              //MORE
        //list <int> aar;               //FEATURE

    public:
        Resultat(Lag *lag);
        Resultat(ifstream & inn);           //DENNE LESER FRA FIL, OK!!!!!

        void skrivVinnere();                //Lager nå?? ja, DONE???
        void plussEnSeier();

        void skrivTilFil(ofstream & ut);    //Works like a charm

        string returnNavn() const { return navn; }  //Returns the name of winners
};

class Bruker{           //Not implemented yet
    private:
        string username;        //The username
        string password;        //The password, hashed
        bool admin;             //If admin

    public:
        
        Bruker(ifstream & inn);

        string returnUsername() const { return username; }  //yes
        string returnPassword() const { return password; }  //and yes
        bool returnAdmin() const { return admin; }

        //sjekkPassord
        //nyBruker
        //sjekkOmAdmin
};

vector <Resultat*> gResultater;         //Holdes the results
vector <Turnering*> gTurneringer;       //Holdes the tournamets
vector <Bruker*> gBrukere;

/*
 *  Definisjon av andre funksjoenr
 */

void skrivTurnering();      //Writes out all the tournamets
void nyTurnering();         //Create a new tournamet
void kjorTurnering();       //Runs a tournament that is created
void shuffleTeams();        //Shuffles the vector with hardcoded teams
void skrivResultater();     //Write out all the
void skrivMenyAdmin();      //Write out the different possibilites
void skrivMenyUser();

void lesFraFilResultater();
void lesFraFilUsers();
//void lesFraFilLag();            //FEATURE
void skrivTilFilResultater();
void skirvTurneringMedNavn();

string loginUser();
void newUser(const bool admin);

bool sjekkAdmin(const string r_name);


int main(){
    char svar;
    string username = "0";
    string t_username;
    bool statusType = false;

    lesFraFilResultater();
    //lesFraFilLag();           //FEATURE
    lesFraFilUsers();

    cout << "\n\nYou are logged in as -Guest-\n"; //Standard

    skrivMenyUser();

    svar = lesChar("What would you like to do?");

    while(svar != 'Q'){
        t_username = "-1";
        switch(svar){
            case 'S' : skrivTurnering();            break;
            case 'N' : nyTurnering();               break;
            case 'K' : kjorTurnering();             break;
            //case 'T' : shuffleTeams();      break;    //check that function works
            case 'R' : skrivResultater();           break;
            case 'H' : skirvTurneringMedNavn();     break;
            case 'L' : t_username = loginUser();    break;
            case 'M' : newUser(username != "0");    break;
        }

        if(t_username != "-1"){
            username = t_username;                  //Det ble logget inn og setter dette til status bar
        }

        if(username != "0"){
            statusType = sjekkAdmin(username);
        }

        if(username != "0"){
            cout << "\n\nYou are logged in as -" << username << endl;
            if(statusType) cout << " | adm-\n\n";
            else cout << "-";
        }else
            cout << "\n\nYou are logged in as -Guest-";

        if(statusType){
            skrivMenyAdmin();
        }else skrivMenyUser();

        svar = lesChar("What would you like to do?");

    }

    skrivTilFilResultater();
}

/*
 *  Public functions for Turnering Bruker aka user
 */

Bruker::Bruker(ifstream & inn){
    int t_rolle;
    inn >> t_rolle; inn.ignore();
    getline(inn, username);
    getline(inn, password);

    cout << username << ": " << t_rolle << endl;

    if(t_rolle == 0) admin = false;
    else admin = true;
    //t_rolle == 1 ? admin = true : admin = false;    //Set admin permissions, if else
}

/*new
 *  Public functions for Turnering class
 */

Turnering::Turnering(){
    slutt = false;
    
    cout << "Add the location of the tournament: "; getline(cin, host);
    //aar = lesInt("Add the year of the tournament", 1970, 2020);               //FEATURE
    for(int i = 0; i < 4; i++) {        //lArranges 4 matches for the tournament
        Kamp *nyKampP; nyKampP = new Kamp(i);
        kamper.push_back(nyKampP);
        cout << "\ncheck\n";
    }
}

int Turnering::hostNameLength(){
    int l_navn = host.length();
    cout << "\nDette er lengden: " << l_navn << endl;
    return(l_navn);
}

void Turnering::skrivNavn(){
    cout << host << ".";
}

void Turnering::skrivData(){
    cout << "\n\nThe location of the tournament: " << host << " <3\n\n";
    for(int i = 0; i < kamper.size(); i++){
        cout << "_____________________________________\n";
        kamper[i]->skrivData();                             //Output the matches
    }
}

void Turnering::kjorTurnering(){
    int antKamper = kamper.size();  //Check how many matches that are about to be played

    spillKamp(kamper.size());       //Playes match (4, 2 or 1 match)
}

void Turnering::goalsForKamp(const int nr){
    kamper[nr]->bestemMaal();
}

void Turnering::spillKamp(const int ant){
    Lag* vinner1; Lag* vinner2; Lag* vinner3; Lag* vinner4; int okei = 0;       //LOLZ, fordi jeg kan

    //Denne if-en under er veldig viktig ettersom den avgjør om det skal lages ny peker

    for(int i = 0; i < kamper.size(); i++){     //Initierer kaper med goals
        goalsForKamp(i);                        // i for å vite hvilket numemr
    }

    if(ant == 1){       //One match
        cout << "\n\n\t\tThe winner of the tournament is...\n";
        vinner1 = kamper[0]->sjekkVinner(); vinner1->skrivData();       //Output winner

        //FIX, this feature does not work, but will not change
        for(int i = 0; i < gResultater.size(); i++){        //Removes tournament
            if(gResultater[i]->returnNavn() == vinner1->returnName()) okei = i;
            break;      //Quit the seeking for name
        }

        if(okei){       //If allready registrered in gResultater +1 victory
            gResultater[okei]->plussEnSeier();
        }else{
            Resultat *nyResultat; nyResultat = new Resultat(vinner1);   //Creates new result object
            gResultater.push_back(nyResultat);
            cout << "\nThe winner has been added to results!\n" << endl;
        }


    }else if(ant == 2){     //2 Matches to be played, same concept
        cout << "\n---Teams remaining\n\n";
        vinner1 = kamper[0]->sjekkVinner();
        vinner2 = kamper[1]->sjekkVinner();
        cout << "Number of matches: " << kamper.size() << endl;

        for(int i = 0; i < kamper.size(); i++) delete kamper[i];
        kamper.clear();

        Kamp *KampP; KampP = new Kamp(vinner1, vinner2);
        kamper.push_back(KampP);

    }else if(ant == 4){     //If 4 matches to be played
        cout << "\n---Teams remaining\n\n";
        vinner1 = kamper[0]->sjekkVinner();
        vinner2 = kamper[1]->sjekkVinner();
        vinner3 = kamper[2]->sjekkVinner();
        vinner4 = kamper[3]->sjekkVinner();

        for(int i = 0; i < kamper.size(); i++) delete kamper[i];
        kamper.clear();

            Kamp *KampP;
            KampP = new Kamp(vinner1, vinner2); kamper.push_back(KampP);
            KampP = new Kamp(vinner3, vinner4); kamper.push_back(KampP);

    }else{
        cout << "\nTournament has been concluded\n";
    }
}

/*
 *  Public functins for Kamp class
 */

Kamp::Kamp(Lag* lag1, Lag* lag2){           //Jeg er ganske bigbrain hvis dette fungerer
    kampLag.push_back(lag1);                //Constructors to make new match
    kampLag.push_back(lag2);
}


void Kamp::bestemMaal(){
    int goals1 = 0, goals2 = 0;
    while(goals1 == goals2){
        goals1 = kampLag[0]->lesGoals();
        goals2 = kampLag[1]->lesGoals();
        cout << ((goals1 == goals2) ? "\nThe teams scored the same amout of goals, read again" : "");
    }
    
}

Lag* Kamp::sjekkVinner(){
    int nr1, nr2;
    //if(kampLag[0]->returnGoals() > kampLag[1]->returnGoals())     //Denne skal egentlig brukes

    nr1 = kampLag[0]->returnGoals();    //Sinze the feature of choosing a winner is not implemented yes
                                            //the winner is allready determined
    nr2 = kampLag[1]->returnGoals();

    //Output winner of match
    if(nr1 > nr2) cout << kampLag[0]->returnName() << " won against " << kampLag[1]->returnName() << endl;
    else cout << kampLag[1]->returnName() << " won against " << kampLag[0]->returnName() << endl;

    if(nr1 > nr2) return kampLag[0];
    else return kampLag[1];

    //NOTE: kan legge inn at man selv sette inn mål når kamper skal spilles, men ikke enda, eller, jeg gjorde det i stedet
}

/*
 *  Public function for "Lag" class. Nothing implementer yet. Constructor created in class
 */

//HER KOMMER DET FUNKSONER

/*
 *  Public function for "Resultat" class. Nothing implementer yet. Constructor created in class
 */

Resultat::Resultat(Lag *lag){
    navn = lag->returnName();
    antSeiere = 1;              //Opprettes bare første gang en objekt lages, gaaaanske sikker på at det vil fungere
    // aar.push_back()          //Får leffes til senere
}

void Resultat::skrivVinnere(){      //Outout winner stats, Results
        cout << left << setw(20) << "Team name:   " << navn << endl;
        cout << left << setw(20) << "Matches won: " << antSeiere << '\n' << endl;
}

void Resultat::plussEnSeier(){      //Needs fix
    antSeiere += 1;
}

Resultat::Resultat(ifstream & inn){
    inn >> antSeiere; inn.ignore();
    getline(inn, navn);
}

void Resultat::skrivTilFil(ofstream & ut){
    ut << antSeiere << " " << navn << "\n";
}

/*
 *  GLOBALE FUNKSJONER
 */


void newUser(const bool admin){

}

string loginUser(){
    string t_username, k_username;
    string t_password, k_password;
    bool t_admin;

    cout << "\nUsername: "; getline(cin, t_username);
    cout << "\nPassword: "; getline(cin, t_password);

    for(int i = 0; i < gBrukere.size(); i++){
            k_username = gBrukere[i]->returnUsername();
            k_password = gBrukere[i]->returnPassword();
            t_admin = gBrukere[i]->returnAdmin();

        if(k_username == t_username){
            if(t_password == k_password){
                cout << "\n\n\t---Innlogging Succsessful---\n";
                
                if(t_admin){
                    cout << "\nYou have superuser priveges\n";
                }else cout << "\nYou are logged in to a regular user\n";

                return k_username;     //return type
            }
                //Loops again if there are more users

        }
    }
    cout << "\n\n\t---Innlogging UnSuccessful---\n\n";
    return "0";
}

bool sjekkAdmin(const string r_name){
    string t_name;
    bool status;
    int ant = gBrukere.size();
    for(int i = 0; i < ant; i++){
        t_name = gBrukere[i]->returnUsername();
        status = gBrukere[i]->returnAdmin();

        if(r_name == t_name){ 
            return (gBrukere[i]->returnAdmin());
        }
    }
    cout << "\tERROR 404\n\n";
    return -1;  //Fant ingen, ERROR, should never happen
}

void lesFraFilUsers(){
    ifstream innfil("Users.dta");
    int antLest = 0;

    if(innfil){
        cout << "\nLeser fra Users.dta";
        while(!innfil.eof()){
            Bruker *n_bruker; n_bruker = new Bruker(innfil);
            gBrukere.push_back(n_bruker);
            antLest++;
        }

        cout << "\nDet er ant brukere lest inn: " << antLest << endl;
    }
}

void skirvTurneringMedNavn(){

    if(!gTurneringer.empty()){
        cout << "\n\nThis is where each tournament is held\n\n";

        for(int i = 0; i < gTurneringer.size(); i++){
            cout << "\nTurnment " << i+1 << " in is held in: ";
            gTurneringer[i]->skrivNavn();
        }
        cout << "\n\n";
    }else
        cout << "\n\nNo Tournaments registered\n\n";
}

void lesFraFilResultater(){
    int ant;
    ifstream innfil("Resultater.dta");

    if(innfil){
        cout << "\nData read from -Resultater.dta-\n";

        innfil >> ant;
        for(int i = 0; i < ant; i++){
            gResultater.push_back(new Resultat(innfil));
        }
    }else
        cout << "\nUnable to read file\n";
}

void skrivTilFilResultater(){
    ofstream utfil("Resultater.dta");

    if(utfil){
        cout << "\nOutputs data to file -Resultater.dta-\n";
        utfil << gResultater.size() << "\n";
        for(int i = 0; i < gResultater.size(); i++){
            gResultater[i]->skrivTilFil(utfil);
        }
    }else
        cout << "\nWas unable to output data to -Resultater.dta-\n";

}

/*                          Legges inn senere
void lesFraFilLag(){
    int ant; string temp;
    ifstream innfil("Resultater.dta");

    if(innfil){
        cout << "\nLeser fra -FootballLag.dta-\n";

        innfil >> ant; innfil.ignore();
        for(int i = 0; i < ant; i++){
            getline(innfil, temp);
            gFotballLag.push_back(temp);
        }
    }else
        cout << "\nFår ikke lest fra fil\n";
}
*/

void skrivResultater(){         //Global function for output of Results
    if(!gResultater.empty()){
        cout << "Previous winners\n\n";
        for(int i = 0; i < gResultater.size(); i++) gResultater[i]->skrivVinnere();
    }else
        cout << "\nNO RESULTS ADDED YET\n";  //Notthing registrated
}

void kjorTurnering(){

    if(!gTurneringer.empty()){
        int nr, kampSize;
        cout << "There are currently: " << gTurneringer.size() << " tournaments running.\n";
        nr = lesInt("Which tournament would you like to run", 1, gTurneringer.size());
        kampSize = gTurneringer[nr-1]->returnKamperSize();      //I think I need this???, .size()??? MORE
        gTurneringer[nr-1]->kjorTurnering();

        if(kampSize == 1){       //Deletes tournamets objekt if finished
            delete gTurneringer[nr-1];                                  //Removes
            gTurneringer[nr-1] = gTurneringer[gTurneringer.size()-1];   //Moves the end to the place where deleted :)
            gTurneringer.pop_back();                                    //Removes end
        }

    }else
        cout << "\nThere are no tournaments running at this time" << endl;
}


void nyTurnering(){

    shuffleTeams();

    Turnering *TurneringP;  TurneringP = new Turnering;

    if(TurneringP->hostNameLength() > 4){
        gTurneringer.push_back(TurneringP);
        cout << "\nhhh: "; gTurneringer.size();
        cout << "Number of elements: " << gTurneringer.size() << endl;
    }else
        cout << "\n\n!Name must be at least 5 charachters long!\n\n";
}

void skrivTurnering(){          //Output tourament
    if(!gTurneringer.empty()){
        for(int i = 0; i < gTurneringer.size(); i++){
            gTurneringer[i]->skrivData();
            cout << "_____________________________________\n";
        }
    }else
        cout << "\nNo tournament is currently running" << endl;
}   

void shuffleTeams(){        //Shuffles the tournamet
    auto rng = std::default_random_engine {};

    shuffle(gFotballLag.begin(), gFotballLag.end(), rng);

    cout << "\nTeams added in a randomised order\n\n";

}

void skrivMenyAdmin(){
    cout << "\n\t---Menu---\n";
    cout << "(N) Create a new tournament\n";
    cout << "(K) Run tournament\n";
    cout << "(S) Current matches per tournament\n";
    cout << "(R) Output results from previous tournaments\n";
    cout << "(H) Write out location for each tournment\n";
    cout << "(L) Login to a user\n";
    cout << "(Q) Quit the program\n\n";
}

void skrivMenyUser(){
    cout << "\n\t---Menu---\n";
    cout << "(S) Current matches per tournament\n";
    cout << "(R) Output results from previous tournaments\n";
    cout << "(H) Write out where each tournment is held\n";
    cout << "(L) Login to a user\n";
    cout << "(Q) Quit the program\n\n";
}