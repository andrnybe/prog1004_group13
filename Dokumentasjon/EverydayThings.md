## Oversikt over (potensielt ikke-) relevant fagstoff

Kildehenvisning  
I løpende tekst: 
```inText
(Norman, 2013, p. page_nr)
```
I kildeliste; 
```Referanseliste
Don Norman. (2013). The Design of Everyday Things(Revised etd.). New York, United States: The Perseus Books Group
```

### Fundamental Principles of Interaction p. 10
*Sitater*
> *"Experience is critical, for it determines how fondly people remembers their intercations." p. 10* <br>
> *"Cognition and emotions are tightly intertwined, which means that the designers must design with both in mind." p. 10* 

For at brukere skal kunne bruke produkter, krever det god *discoverability*. Discoverability stammer fra bruken av fem pysykologiske konsepter:
- Affordance
- Signefiers
- Constraints
- Mapping
- Feedback  


Men også et sjette prinsipp: Conceptial model
> *"It is the conceptual model that provides true understanding."  p. 10*

Se forklaring: System image (p. 31)
#### Affordance (p. 10)
> *"… refers to the relationship between a physical object and a person… " p. 11*  
> *"An affordance is a relationship between the properties of an object and the capabilities of the agent that determine just how the object could be used." p. 11*  

Eksempel: A chair affords support, thus affords sitting. Is the chair small or lightweight? Then it could afford lifting. Note: Depenedent of the individual's capabilities. Lifting something heavy as an adult is way easier than as a child. 
	
Vi tenker ofte på slikt som egenskaper til et objekt. Kan løftes. Kan sittes på. Kan ditt, kan datt. Men merk at det er kun fordi vi er i stand til å se mulighetene. 
	
Vi kan bruke Wireshark for å overvåke nettverkstrafikk. Men det er kun en kombinasjon mellom vår evne (eganskap) til å bruke Wireshark sammen med at en maskin har Wireshark installert. Om du lar en annen person som ikke vet om wirehark på maskinen så vil ikke personen kunne overvåke nettverkstrafikken, selv om maskinen fremdeles har Wireshark installert. Fordi forholdet mellom hva maskinen kan i forhold til hva personen kan overlapper ikke på det punktet. 

#### Signefiers (p. 13)
> *"Affordances determine what actions are possible. Signifiers communicate where the action should take place" p. 14*

Handler om å vise at en handling er mulig. 
E.g. i programmering når vi skriver en tekst før vi ber om input. På denne måten gjør vi brukeren oppmerksom på funksjonen av å kunne skrive til programmet. Merk at når vi bruker 'cin >>' el. så er programmet i stand til å lese inn data selv, problemet er at brukeren vet kanskje ikke det helt.  
Når vi bruker C/C++ benytter vi oss av lesInt(...) funksjonen der vi kan legge med en tekst. Denne teksten tydeliggjør det at programmet venter på input fra brukeren. Husk at programmets evne til å lese inn brukerinput gjelder selv om brukeren er klar over det eller ikke.  

#### Constraints (ch. 3 & 4)
Se om Constraints under
#### Mapping (p. 20)
> *"Mapping is a technical term ... meaning the relationship between the elemenets of two sets of things." p. 20* 
> *"Mapping is an imporatant concept in the design and layout of contorls and display" p. 21*

Les mer under [natural mapping](https://gitlab.stud.idi.ntnu.no/eskillr/prog1004_group13/-/blob/main/Dokumentasjon/EverydayThings.md#natural-mapping-p-113) under.
#### Feedback (p. 23)
> *"Feedback - communicating the results of an action..."p. 23*

Bekrefter handlinger fra bruker har blitt registrert. Slike tilbakemeldinger bør komme med en gang, da venting kan føre til ubehag og usikkerhet rettet mot programmets funksjonalitet. 
e.g. Når du trykker på knappen forran heisen for at den skal komme til tid etasje. Om knappen lyser så vet du at din handling har blitt registrert, du har mottatt rask feedback. Men, om den ikke lyser med en gang vil du starte å lure på om det har blitt registrert, du har nå mottatt en treg feedback. 

Når det gjelder programmet vårt, så er det et CLI program som kjører kun på en CPU-string. Det vil si at respons er rettet mot funksjonalitet er veldig rask til å begynne med. 
Eksempler er om brukeren klarte å logge inn (access granted vs. access denied) og lingnende.

#### System image (p. 31)
*In terms of creating a conceptial model, combined information available to us e.g. apperance.*  
	
>*"The user's conceptual model comes from the system image, through interaction with the product, reading, seraching for online information and from whatever manuals are provided" p. 31*
	
i.e. The designers do not communicate, their products do. 
	
>*"Good conceptual models are the key to understandable, enjoyable products: Good communication is key to good conceptual models." p. 32*  
	
Burde først hatt en brukerundersøkelse UTEN en framvisning av wireframes først (vent med å vise den). På denne måten så vil brukeren datte et system image ut ifra forventinger brukeren har selv. Deres system image vil  da bli sammenlignet med vår wireframe og gi direkte feedback om det var hva de forventet. Med åpninger for hvorfor / hvorfor ikke. 


#### Design challenges p. 34
> *"The hard part is to convince people to understand the veiwpoint of the others, to abonden their diciplinary viewpoints and to think of the design from the viewpoint of the person who buys the products and those who use it, often different people." p. 35*
	
Hver side av skapelsen av et produkt mener at sin kontribusjon er viktigst / best. Helpdesk ansvarlig kan mene at brukerstøtte skal ta prioritering, mens programmerere vil muligens prioritere høy grad av funksjonalitet og kompleksitet. De fokuserer på hva de er gode bå, men glemmer å se at det de lager er kanskje ikke hva kjøperen spør etter, samt ei hva brukeren trenger produktet for. Se på dette [bilde](https://keremkosaner.files.wordpress.com/2008/04/softwaredevelopment.gif) for et visuelt eksempel. Ulike avedlinger kan ønske å prioritere forskellig, en avedeling ser på brukbarhet  og forståelse, mens en annen vil passe på kostbarhet og miljøvennlighet, imens den siste ønsker at produktet er attraktivt. Det er viktig å finne et design som oppfyller alle krav for alle avedlinger. Men viktigst av alt, at brukeren av produktet sine krav blir oppnådd. 


#### Seven Stages of Action p. 40
Hentet fra side 40- 41:
1. Goal (form the goal)
	- Hva ønsker du å oppnå? 
	- E.g. Ønsker bedre belysning for å lese en bok
2. Plan (the action)
	- Bestem deg for en måte å oppnå målet ditt
	- E.g. Slå på en lampe i nærheten / flytte deg til et bedre belyst området. 
3. Specify (an action sequence)
	- Spesifiser hvilke handlinger trengs for å gjøre handlingen
	- E.g. Posisjoner deg mot lampen. Dra å lampesnoren / slå på lysbryteren.
4. Perform (the action sequence)
	- Utfør planlagt handlingene fra punkt 3.
5. Percieve (the state of the world)
	- Oppfatt endringen som ble gjort i verden. Hva slags effekt hadde punkt 4
	- E.g. Tilstrekkelig belysning passende ditt behov / ikke nok lys / ingen belysning.
6. Interpret (the percetption)
	- Forstå endringen og hvorfor  det skjedde
7. Compare (the outcome with the goal)
	- Sammenling det som skjedde med hva du ønsket å oppnå.
	- E.g. Ble det nok lys som var ønsket

Dette kan bli brukt sammen med splitt og hersk-algoritmen for å forminske store problemer til mindre håndterbare problemer.


#### Storytelling p. 56
> *"Stories resonate with our experiences and provide examples of new instances. From our experiences and the stories of others we tend to form generalizations about the way people behave and things work". p. 57*  
	
Stories er individers forklaringer på hvordan årsak og virkning hører sammen. La oss si at en person får A i alle fag. En person kan se det som at personen bare er født heldig og er super smart. Dette gir mening for personen, men det kan godt hende at det er en samling av utrolig mange faktorer, hvor intelligens er en delfaktor, men utgjør ikke alt.

#### Seven Fundamental Design Principles (p. 71 - 72)
Vi trenger å lage et produkt som følger The Seven Stages of Action. Men hvordan kan vi det? The seven fundamental design principles er løsning gjøre nettopp det. Merk at dette er noe som har blitt snakket om i tidligere forelesning, men at kilden benyttet var en oppsummering. Så formuleringen kan er litt forskjellig, men budskapet er likt.

1. Discoverability
    - It is possible to determine what actions are possible and rthe current state of the device.
2. Feedback
    - There is full and continuous information about the results of actions and the current state of the product or service. After an action has been executed, it is easy to etermine the new state.
3. Conceptual model
    - The design projects all the information needed to create a good conceptual model of the system, leading to understanding and a feeling of control. The conceptual model enances both discoverability and evalutation of results.
4. Affordance
    - The proper affordances exissts to make he desired actions possible.
5. Signifiers
    - Effective use of signifiers ensures discoverability and that the feedback is well communicated and intelligible.
6. Mapping
    - The relationship between controls and their actions follows the principles of good mapping, enganced as much as possible through spatial layour and temporal contiguity.
7. Constraints
    - Providing physical, logical, semantic and cultural constraints guides actions and ease Interpretation.

#### Natural mapping (p. 113)
> *"Mapping (...) provides a good examle of the power of combining knowledge in the world with that in the head" p. 113*

Måten å bruke produkters funksjoner bør være enkelt og entydelig representert i design / knapper/ kontrollere. Eks. hvordan en heis kan ha loddrette knapper 3, 2, 1 for å representere etasjene på en oversiktelig måte.

> *"Natural mappings are those where the relationship between the controls and the object to be controlled (...) is obvious" p. 115*

Forslag: Bruk tall i stedenfor bokstaver for brukerinput. På den måten vil listen over mulige alternativer passe tallene på tastaturet. Samtidig som at brukeren ikke behøver å se over tastatuer for å velge den korrekte knappen på tastaturet.

Tre typer (p. 115):
- Best mapping
	- Controls are mounted directly on the item to be controlled.
	- Heisknapper, komfyr knotter.
- Secont-best mapping
	- Controls are as close as possible to the object to be controlled.
- Third-best mapping
	- Controls are arranged in the same spatial configuration as the objects to be controlled. 
	- Lysbrytere

#### Constraints (p. 125)
Det er fire forskjellige begrensninger (p. 125):
- Physical 
	- *Physical limitations constrain possible operations. (p.125)* 
	- E.g. lesInt(...) funksjoner fra PROG som begrenser lovlige verdier å bli tastet inn.
- Cultural (p. 128)
	- *Each culture has a set of allowable actions for social situations (p. 128)*
	- Unngå bruken av 'Æ', 'Ø', 'Å'? Bruk av engelsk?
- Semantic
	- *Semantic constraints are those that rely upon the meaning of the situation to control the set of possible actions." (. 129)*
- Logical 
	- *"... a logical logical relationship between the spatial or functional layout of components and the things that they affect or are affected by"(p. 130)*
	- Logisk begrensninger benyttes for å oppnå en *natural mapping*.


#### Constraint & behaviour (p. 141 - 145)
Vi kan benytte begrensninger for å påvirke handlinger mot et ønsket mål.
- Forcing functions
	- En form for physical constraint.
	- Situasjoner som stanser at programmet kjører videre etter en feil.
	- E.g. if-else som sjekker innhold/ oppfyller krav før programmet kan kjøre. 
- Interlocks
	- Krever at operasjoner må bli utført i en gitt rekkefølge, og at et steg kan ikke skje før det tidligere steget er konkludert. 
	- E.g. I programmering så leser vi inn data fra filer før vi har muligheten til å bruke kommandoer som benytter data-filene.
	- E.g. En mikrobølgeovn har et krav om at døren er lukket for å kjøre. Om du åpner det mens den kjører så vil den slå seg selv av.
- Lock-ins
	- Holder en prosess aktiv så den ikke blir stengt før den skal. 
	- E.g. Gå ut av et program kan føre til at data ikke blir lagret riktig. I programmer er det ofte meldinger som spør om du ønsker å lukke programmet på en slik utrygg måte.  
- Lockouts
	- Tjenester er utilgjengelig frem til krav for bruk er oppfylt.

#### Sound (p. 155)

> *"... sound can provide information available in no other way" p. 155*
> *"Sound is tricky. It can annoy and distract as easily as it can aid" p. 156*
Vi kan benytte lyder for å varsle brukeren om forskjellige aspekter: Feil inntasting, turnering har gått igjennom riktig osv.
I vårt program blir ikke lyd benyttet. Men det er greit å vise forståelse for at det vil ha en innvirkning på brukeren. Ikke bare kan det bli brukt for underholdning, men også informativt. 
Kan nevne dette mot videre implementeringer av tjenesten.  

#### Slips and mistakes, (p. 170)

To typer feil: Slips and mistakes

- Slips (to hovedtyper) p. 171
	- *... when a person intends to do one actions and ends up doing something else. p. 171*
	- **Action-based** fokuserer på at handlingen var feil.
	- **Memory-lapse** hvor personen har glemt eller vet ikke hvordan en utfører en handling og blir derfor feil.

- Mistakes
	- *... occures when the wrong goal is established or the wrong plan is formed p. 171*
	- **Rule-based** hvor diagnoseringen er riktig, men løsningen baserer seg på *rules* som er feil. Målet blir så ikke oppnådd.
	- **Knoweldge-based** hvor diagnoseringen er feil. 
	- **Memory-lapse** hvor et steg er blitt glemt og derfor ikke gjort.


#### Solving the correct problem (p. 218)
> *"A brilliant solution to the wrong problem can be wrose than no solution at all: Solve the correct problem" p. 218*

Forsøk å frostå hva som er det faktiske problemet. Få en forståelse om hva som faktisk behøves. Ikke bare av klienten, men brukeren av systemet. 
Dette går litt tilbake til [**Design Challenges**](https://gitlab.stud.idi.ntnu.no/eskillr/prog1004_group13/-/blob/main/Dokumentasjon/EverydayThings.md#design-challenges-p-34) 

For å finne hovedproblemet kan vi benytte teknikken *Five Whys*. Hovedsakelig er dette en metode for å gå grundigere inn i et problem og ikke nøye seg med et overordnet svar på et problem. 
Fremgangsmåten er at du spør hvorfor hendelse X skjedde. Etter at du finner ut at hendelse X oppsto av problem Y, så spør du igjen om hvorfor hendelse Y skjedde.
Case: En PC krasjer etter at den ble oppgradert til Win 11:
```Case  
Hvorfor kraser PC-en? På grunn av Win 11 oppgraderingen.
Hvordan forårsaket oppgraderingen krasingen? På grunn av de nye skjermdriverne ikke er kompatible med operativsystemet.  
Hvorfor er ikke skjermdriverne kompatible? På grunn av ...  
Hvorfor ... ? På grunn av  ...   
Hvorfor ... ? På grunn av  ...  
```
Til slutt står du igjen en god oversikt over hvor problemet ligger, og ikke bare at problemet er Windows 11.

**Obs!** Det er vel å merke at en slik analyse kan gi forskjellige svar / løsninger ut ifra hvem som analyserer. Så at forskjellige analyser kan gi forskjellige resultater. 

Design thinking
- Tenkte seg til hva problemet er og flere potensielle løsninger før det blir lagt en løsning.
- To relevante verktøy å snakke om er *Human-centered design* og *double-diamond diverge-converge model.

Human-centered design (HCD) er en prosess som påser at: 
- brukerens behov er møtt
- produktet er forstålig og brukbarhet
- produktet gjør hva den ble designet for
- brukerne er fornøyde med produktet.


> *"HCD is a procedure for addressing these requierments, but with emphasis on two things: solving the right problem, and doing so in a way that meets human needs and capabilities." p. 219*

The Double-Diamond Model of Design (p. 220)\
Den tar for seg å finne det korrekte problemet, og ut ifra dette, finne den korrekte løsningen.

Nyttig og tydelig fordeling av fokus og ressurser, hvor alle kan komme med innspill, før de så finner en felles problemstilling. Deretter kan fokuset bli delt på å finne forskjellige potensielle løsninger før fokuset igjen kommer sammen for å bestemme seg for en felles løsning.  
Hvorfor: 
- Visuell fremgang som f.eks. prosjektledere kan vise til investorer. 
- Muligheter for å gå tilbake til et tidligere steg om nødvendig, og da ha tidligere data å arbeide ut ifra.
- Ressurser blir enkelt og effektivt fordelt etter behov.
- Utviklere får en mulighet til å arbeide fritt, mens de fremdeles følger tidsplanen og budsjettet.

Går ut på fire steg: 
1. Discoverability (diverge)
	- Få en oversikt over problemet, alle tilhørende underproblemer og hva som behøves for å løse dem. 
2. Define (converge)
	- Lag en problemstilling ut ifra oversikten av problemet over.
3. Develop (diverge)
	- Lag potensielle løsninger
4. Deliver (converge)
	- Bli enige om en felles løsning.

Merk: diverge-converge delen og double-diamond modellen kommer av vi først splitter opp fokuset på mange ulike problemer, før vi så samler oss inn til en felles problemstilling. Deretter splittes vi opp igjen for å undersøke flere løsningsforslag, før vi så møtes til en felles løsning. Denne splittingen og sammenslåingen skaper diamant formen / gir den en diverge-converge form.

The Human-Centered Design Process (221)
Blir brukt sammen med The double-diamond diverge-converge prosessen. 
Går ut på fire steg:
1. Observation
	- Innhent informasjon om målgruppen / brukeren.
	- Behov, interesser, motiver, alder, utdanning osv. (merk at ikke alt er nødvendig)
	- Generelt overblikk eller en analysert undersøkelse 
	- Design research versus market research (p. 224)
		- Design: Fokuserer på hva brukeren behøver et produkt for 
		- Marketing: Fokuserer på hvem som vil kjøpe produktet
2. Idea generation (ideation)
	- Skape potensielle løsninger¨
	- Relatert til idemyldring / brainstorming, men følger to regler
		- Frem mange forskjellige løsninger, ikke slå deg fast med noen få
		- Vær kreativ og ikke vær redd for at forslag er rare løsninger. Selv om løsningen ikke passer, kan deler av bidra til nye ideer igjen.
3. Prototyping
	- Lag en prototype for hver løsning. Dette kan være skisser, noe lagd gjennom excel-regneark / word / powerpoint, pappmodeller osv. 
	- Keep it simple!  
4. Testing (p. 228)
	- Lag tester på et format som passer bruket. 
		- Inkluder en mulighet for brukervalg hvor programmet har brukervalg.
			- Om programmet skal ha en rullegardinliste, se at prototypen din har en form for en rullegardinliste. 
		- Test prototyper designet for grupper, til grupper, og enkeltbrukere til enkeltbrukere. 
		- Kan kanskje ha et par, en som opererer prototypen, og en annen som har manualen. Dette kan være ideelt for å teste sammenhengen mellom forståelse av prototypen og klarheten av manualen. Her er det greit at testerne får være i et eget rom og kan diskutere ideer i fred. Fremgangen kan bli tatt opp på video, og bli vist senere sammen med parret for å spørre flere spørsmål. 

- Iteration 
	- En viktig rolle rettet mot kontinuerlig forbedring av produktet. 
	- *"Fail frequently, fail fast" p. 229*
		- Lær av feilene og forbedre systemet.

#### Cometetive forces (p. 259)
Stort press på utviklingen av produkter, og kun få forskjeller som skiller produkter. De viktigste er da naturligvis pris, trekk og kvalitet. Tid, og hvor fort et pordukt kommer til markedet er også viktig. Da noen produkter kan gjerne være helt nye, men komme opp flere steder samtidig på grunn av det man kaller *zeitgeist*, eller *spirit of the time*. Det er veldig viktig å være tidlig ute for å få et forsprang på potensielle investorer som hadde vært villige til å sponse utviklingen av nye produkter. 


#### Innovation (p. 279)
To former av innovasjon:
	- Incremental
		- Små endringer over lang tid
		- iPhone 8 -> iPhone 9. 
	- Radical
		- Store endringer på kort tid
		- Lanseringen av den første iPhone-en, den første bilen osv.
		- Lanseringen av nye tjenester som GPS.

#### The design of Everyday Things (p. 282)
> *"Technology changes rapidly, people and culture change slowly" p. 282*

Selv om teknologi endrer seg hele tiden, og mennesker endrer oss gradvis, vil de grunnleggende aktivitetene våre forbli prisnippielt de samme. Som for eksempel hvordan vi hører på musikk, kommuniserer med mennesker, spiser mat osv. Så om man er interessert i å lage et nytt produkt, så vil disse prinsippielle behovene våre eksistere langt fram i menneskets fremtid.


#### Moral obligations (p. 291)
> *"We are surronded with objects of desire, not objects of use." p. 291*
> *"Needless features, needless models: Good for business, bad for the enviroment" p. 291*
En måte opprettholde salg på er gjennom at produktet slutter å fungere, og at brukeren er nødt til å kjøpe nytt. Om salg kommer av at et produkt kan bli solgt kun en gang, er det viktig at produktet blir dårlig etter en stund. Dette har vi sett i nyere tid med Apple som gjorde eldre modeller tregere for å oppfordre kjøpet av nyere modeller (eller skifte ut gamle komponenter). 

En annen metode for å oppretthodle salg på er en abonnementsmodell som tilbyr tjenester etter månedlig betaling. Dette forsikrer kvalitet, funksjonalitet kompatibilitet og mye mer. 


#### Design thinking and thinking about design (p. 293)
> *"A design that people do not purchase is a failed design, no matter how great the design team might consider it" p. 293*

Det er viktig at produktet blir solgt, og blir brukt.
